# Use the Rust official image as a parent image
FROM mcr.microsoft.com/devcontainers/rust:0-1-bullseye

# Install lld linker to improve build times
RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install clang lld \
    && apt-get autoremove -y && apt-get clean -y

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Set environment variable to use lld linker
ENV RUSTFLAGS="-C link-arg=-fuse-ld=lld"

# Build the Rust app using the release profile
RUN cargo build --release

# Expose port 8080 to the outside world
EXPOSE 8080

# Command to run the executable, replace `actix_web_app` with your actual binary name if different
CMD ["./target/release/actix_web_app"]
