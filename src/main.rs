use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};
use std::sync::Mutex;

#[derive(Serialize, Deserialize, Clone)] // Ensure Book can be cloned and (de)serialized
struct Book {
    id: u32,
    title: String,
    author: String,
}

// Shared state across handlers, using Mutex for thread-safe mutation
struct AppState {
    books: Mutex<Vec<Book>>,
}

// Handlers
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Welcome to the Bookstore!")
}

async fn add_book(book: web::Json<Book>, data: web::Data<AppState>) -> impl Responder {
    let mut books = data.books.lock().unwrap(); // Lock on the books vector
    books.push(book.into_inner()); // Add new book
    HttpResponse::Ok().json(books.clone()) // Clone the data for serialization
}

async fn add_book_query(info: web::Query<Book>, data: web::Data<AppState>) -> impl Responder {
    let mut books = data.books.lock().unwrap();
    books.push(Book {
        id: info.id,
        title: info.title.clone(),
        author: info.author.clone(),
    });
    HttpResponse::Ok().json(books.clone())
}

async fn get_books(data: web::Data<AppState>) -> impl Responder {
    let books = data.books.lock().unwrap(); // Lock on the books vector
    HttpResponse::Ok().json(books.clone()) // Correctly clone the data for serialization
}

async fn get_book(info: web::Path<u32>, data: web::Data<AppState>) -> impl Responder {
    let books = data.books.lock().unwrap(); // Lock on the books vector
    if let Some(book) = books.iter().find(|book| book.id == *info) {
        HttpResponse::Ok().json(book.clone()) // Correctly clone the found book
    } else {
        HttpResponse::NotFound().body("Book not found")
    }
}

async fn delete_book(info: web::Path<u32>, data: web::Data<AppState>) -> impl Responder {
    let mut books = data.books.lock().unwrap(); // Lock on the books vector
    if let Some(pos) = books.iter().position(|book| book.id == *info) {
        books.remove(pos); // Remove the book from the vector
        HttpResponse::Ok().json(books.clone()) // Correctly clone the data for serialization
    } else {
        HttpResponse::NotFound().body("Book not found")
    }
}

async fn delete_book_query(info: web::Query<Book>, data: web::Data<AppState>) -> impl Responder {
    let mut books = data.books.lock().unwrap(); // Lock on the books vector
    books.retain(|book| book.id != info.id); // Retain only books that don't match the given ID
    HttpResponse::Ok().json(books.clone()) // Respond with the updated list of books
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let data = web::Data::new(AppState {
        books: Mutex::new(vec![]), // Initialize the shared state
    });

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone()) // Add the shared data to the app
            .route("/", web::get().to(index)) // Handle requests to the root URL
            .route("/books", web::post().to(add_book))
            .route("/books", web::get().to(get_books))
            .route("/books/{id}", web::get().to(get_book))
            .route("/books/{id}", web::delete().to(delete_book))
            .route("/add_book", web::get().to(add_book_query)) // New route for adding a book via GET
            .route("/delete_book", web::get().to(delete_book_query)) // New route for deleting a book via GET
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
