# Rust Actix Bookstore Service

## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation](#installation)
4. [Usage](#usage)
5. [Development Process](#development-process)

## Deliverables

![Docker Software Screenshot](/Screenshot_2024-02-28_at_10.26.18_PM.png
)

![Terminal Build Screenshot](/Screenshot_2024-02-28_at_10.26.38_PM.png
)

## Description

This project is a Rust-based web service utilizing the Actix framework. Designed as a straightforward bookstore management system, it enables users to add, view, update, and delete book entries. The service employs a shared, concurrent state, showcasing CRUD operations in a RESTful approach, complemented by additional functionality for data manipulation via URL query parameters.

## Installation

To run this project locally, ensure Rust, Cargo, and Docker are installed on your system. Follow these installation steps:

1. Clone the repository:
git clone https://your-repository-url.git
cd actix_web_app

2. Build the Docker image:
docker build -t actix_web_app .


## Usage

After building and running the Docker container, interact with the service using these endpoints:

- **Homepage (`/`):** Navigate to `http://localhost:8080/` in your browser to see a welcome message.
- **Add Book (`POST /books`):** Utilize tools like Postman or cURL to submit POST requests to add new books.
- **View Books (`GET /books`):** Access `http://localhost:8080/books` to list all available books.
- **View Book (`GET /books/{id}`):** Fetch details of a specific book by its ID at `http://localhost:8080/books/{id}`.
- **Delete Book (`DELETE /books/{id}`):** Use DELETE requests to remove a book by its ID.

To start the service within a Docker container:

docker run -d -p 8080:8080 actix_web_app

## Contributing

Contributions to this project are encouraged:

1. Fork the repository.
2. Create a new feature branch.
3. Commit your changes.
4. Push to the branch.
5. Create a new Pull Request.

## Development Process

Development steps included:

1. **Setup:** Establishing the Rust and Actix framework environment.
2. **Data Modeling:** Defining the `Book` structure and implementing CRUD functionalities.
3. **Endpoints:** Crafting routes and handlers for bookstore operations.
4. **UI Development:** (Optional) Adding HTML templates or React for the interface.
5. **Containerization:** Writing a Dockerfile for easy app deployment.
6. **Testing:** Ensuring functionality through comprehensive tests.
7. **Documentation:** Crafting this detailed guide..


